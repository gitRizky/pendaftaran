import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DataBaseHelper {
  String dataBaseName = "db_customer";
  String tableName = "data_customer";

  Future<Database> initDatabase() async {
    return openDatabase(join(await getDatabasesPath(), "$dataBaseName"),
        version: 1, onCreate: createTable);
  }

  void createTable(Database db, int version) async {
    String syntaxQuery = '''CREATE TABLE "$tableName" (
	"id"	INTEGER,
	"foto_path" TEXT,
	"nik"	TEXT,
	"nama"	TEXT,
	"tanggal_lahir"	TEXT,
	"alamat"	TEXT,
	"nomor_telpon"	TEXT,
	"otp"	TEXT,
	"status_kepegawaian"	TEXT,
	"gaji"	TEXT,
	"bank"	TEXT,
	"sales_respon"	TEXT,
	"kondisi" TEXT,
	PRIMARY KEY("id" AUTOINCREMENT));''';
    await db.execute(syntaxQuery);
  }
}
