import 'package:soalmobiletest/models/db_customer_model.dart';
import 'package:soalmobiletest/utilities/database/database_helper.dart';
import 'package:sqflite/sqflite.dart';

class DbRepo {
  // CRUD, CREATE/INSERT, READ, UPDATE/ EDIT, DELETE
  DataBaseHelper _databaseHelper = new DataBaseHelper();
  Future<int> inserData(DbCustomerModel model) async {
    final Database db = await _databaseHelper.initDatabase();
    final result = await db.insert(_databaseHelper.tableName, model.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return result;
  }

  //read all data mahasiswa
  Future<List<DbCustomerModel>> readData() async {
    final Database db = await _databaseHelper.initDatabase();
    final List<Map<String, dynamic>> datas =
        await db.query(_databaseHelper.tableName);
    return List.generate(datas.length, (index) {
      return DbCustomerModel.fromMap(datas[index]);
    });
  }

  Future<int> updateData(DbCustomerModel model) async {
    final Database db = await _databaseHelper.initDatabase();
    final result = await db.update(_databaseHelper.tableName, model.toMap(),
        //parameter kondisi
        where: "id = ?",
        //valuenya
        whereArgs: [model.id]);

    return result;
  }

  //delete data mahasiswa
  Future<int> deleteData(DbCustomerModel model) async {
    final Database db = await _databaseHelper.initDatabase();

    final result = await db.delete(_databaseHelper.tableName,
        where: 'id = ?', whereArgs: [model.id]);
    return result;
  }

  //Search data by name
  Future<List<DbCustomerModel>> searchData(String keyword) async {
    final Database db = await _databaseHelper.initDatabase();
    String rawQuery =
        'SELECT * FROM ${_databaseHelper.tableName} WHERE nama like "%$keyword%"';
    final List<Map<String, dynamic>> datas = await db.rawQuery(rawQuery);
    final result = List.generate(datas.length, (index) {
      return DbCustomerModel.fromMap(datas[index]);
    });

    return result;
  }
}
