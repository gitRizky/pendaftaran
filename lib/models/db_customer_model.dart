class DbCustomerModel {
  int id;
  String foto_path;
  String nik;
  String nama;
  String tanggal_lahir;
  String alamat;
  String nomor_telpon;
  String otp;
  String status_kepegawaian;
  String gaji;
  String bank;
  String sales_respon;
  String kondisi;

  DbCustomerModel(
      {this.id,
      this.foto_path,
      this.nik,
      this.nama,
      this.tanggal_lahir,
      this.alamat,
      this.nomor_telpon,
      this.otp,
      this.status_kepegawaian,
      this.gaji,
      this.bank,
      this.sales_respon,
      this.kondisi});

  Map<String, dynamic> toMap() {
    Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = id;
    data['foto_path'] = foto_path;
    data['nik'] = nik;
    data['nama'] = nama;
    data['tanggal_lahir'] = tanggal_lahir;
    data['alamat'] = alamat;
    data['nomor_telpon'] = nomor_telpon;
    data['otp'] = otp;
    data['status_kepegawaian'] = status_kepegawaian;
    data['gaji'] = gaji;
    data['bank'] = bank;
    data['sales_respon'] = sales_respon;
    data['kondisi'] = kondisi;
    return data;
  }

  DbCustomerModel.fromMap(Map<String, dynamic> data) {
    this.id = data['id'];
    this.foto_path = data['foto_path'];
    this.nik = data['nik'];
    this.nama = data['nama'];
    this.tanggal_lahir = data['tanggal_lahir'];
    this.alamat = data['alamat'];
    this.nomor_telpon = data['nomor_telpon'];
    this.otp = data['otp'];
    this.status_kepegawaian = data['status_kepegawaian'];
    this.gaji = data['gaji'];
    this.bank = data['bank'];
    this.sales_respon = data['sales_respon'];
    this.kondisi = data['kondisi'];
  }
}
