import 'package:flutter/material.dart';
import 'package:soalmobiletest/view/list_data.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      home: ListData(),
      // routes: {Routes.addcustomer: (context) => AddCustomer()},
    );
  }
}
