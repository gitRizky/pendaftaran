import 'package:soalmobiletest/models/db_customer_model.dart';
import 'package:soalmobiletest/utilities/repository/db_repo.dart';

class DbCustomerViewModel {
  DbCustomerViewModel();

  Future<int> insertData(DbCustomerModel model) async {
    return await DbRepo().inserData(model);
  }

  Future<List<DbCustomerModel>> readData() async {
    return await DbRepo().readData();
  }

  Future<int> updateData(DbCustomerModel model) async {
    return await DbRepo().updateData(model);
  }

  Future<int> deleteData(DbCustomerModel model) async {
    return await DbRepo().deleteData(model);
  }

  Future<List<DbCustomerModel>> searchData(String keyword) async {
    return await DbRepo().searchData(keyword);
  }
}
