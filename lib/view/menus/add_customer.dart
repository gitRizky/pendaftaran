import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:soalmobiletest/component/costum_confirmation_dialog.dart';
import 'package:soalmobiletest/models/db_customer_model.dart';
import 'package:soalmobiletest/viewmodel/db_customer_viewmodel.dart';

class AddCustomer extends StatefulWidget {
  @override
  _AddCustomerState createState() => _AddCustomerState();
}

class _AddCustomerState extends State<AddCustomer> {
  TextEditingController _nik = new TextEditingController();
  TextEditingController _nama = new TextEditingController();
  TextEditingController _dob = new TextEditingController();
  TextEditingController _alamat = new TextEditingController();
  TextEditingController _telpon = new TextEditingController();
  TextEditingController _otp = new TextEditingController();
  TextEditingController _gaji = new TextEditingController();
  TextEditingController _respon = new TextEditingController();
  int statusRadioValue;
  String _status;
  String _bank;
  String _fotoPath;
  File _foto;
  List itemBank = ["BNI", "BTPN", "BRI"];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration:
              BoxDecoration(border: Border.all(width: 10, color: Colors.grey)),
          child: GestureDetector(
            child: ListView(
              padding: EdgeInsets.all(20),
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      GestureDetector(
                        onTap: getImageFromCamera,
                        child: _foto == null
                            ? CircleAvatar(
                                radius: 40,
                                child: Icon(CupertinoIcons.camera),
                              )
                            : CircleAvatar(
                                radius: 40,
                                backgroundImage: FileImage(_foto),
                              ),
                      ),
                      Text("Ambil Foto Selfie")
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("NIK"),
                      TextField(
                        // maxLength: 16,
                        keyboardType: TextInputType.number,
                        controller: _nik,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Nama"),
                      TextField(
                        controller: _nama,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Tanggal Lahir"),
                      TextField(
                        controller: _dob,
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                          suffixIcon: Icon(CupertinoIcons.calendar),
                        ),
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());
                          datePicker(context);
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Alamat"),
                      TextField(
                        controller: _alamat,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("No. Telpon"),
                      TextField(
                        controller: _telpon,
                        keyboardType: TextInputType.phone,
                      ),
                    ],
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Column(
                //     crossAxisAlignment: CrossAxisAlignment.start,
                //     children: [
                //       Text("Kode OTP"),
                //       TextField(
                //         controller: _otp,
                //         keyboardType: TextInputType.number,
                //       ),
                //     ],
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Status Kepegawaian"),
                      Row(
                        children: [
                          new Radio(
                            value: 0,
                            groupValue: statusRadioValue,
                            onChanged: handleRadio,
                          ),
                          Text("Aktif Bekerja"),
                          SizedBox(
                            width: 5.0,
                          ),
                          new Radio(
                            value: 1,
                            groupValue: statusRadioValue,
                            onChanged: handleRadio,
                          ),
                          Text("Pensiun"),
                        ],
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Gaji Aktif/Pensiun"),
                      TextField(
                        controller: _gaji,
                        keyboardType: TextInputType.number,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Pembayaran Gaji dengan Bank"),
                      Container(
                        child: DropdownButton(
                          hint: Text("--Pilih--"),
                          value: _bank,
                          items: itemBank.map((e) {
                            return DropdownMenuItem(
                              child: Text(e),
                              value: e,
                            );
                          }).toList(),
                          onChanged: (e) {
                            setState(() {
                              _bank = e;
                            });
                          },
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Sales Respon"),
                      TextField(
                        controller: _respon,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey)),
                      onPressed: () {
                        validasiInput();
                      },
                      child: Text(
                        "Simpan",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void validasiInput() {
    if (_nik.text.isEmpty ||
        _fotoPath == null ||
        _nama.text.isEmpty ||
        _dob.text.isEmpty ||
        _alamat.text.isEmpty ||
        _telpon.text.isEmpty ||
        // _otp.text.isEmpty ||
        _status == null ||
        _gaji.text.isEmpty ||
        _bank == null ||
        _respon.text.isEmpty) {
      Fluttertoast.showToast(
          msg: "Semua Data Harus Diisi",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER);
    } else {
      var confirmationDialog = CustomConfirmationDialog(
        title: "Apakah Anda Yakin Telah Mengisi Data Dengan Benar?",
        message:
            "Setiap data yang anda input akan kami simpan dan mengubahnya di data kantor pusat. Perhatikan kembali data - data yang anda input.",
        yes: "Simpan Data",
        no: "Mengedit Lagi",
        pressYes: () {
          simpan();
        },
        pressNo: () {
          Navigator.of(context).pop();
        },
      );
      showDialog(context: context, builder: (_) => confirmationDialog);
    }
  }

  void simpan() async {
    DbCustomerModel model = new DbCustomerModel(
      nik: _nik.text,
      foto_path: _fotoPath,
      nama: _nama.text,
      tanggal_lahir: _dob.text,
      alamat: _alamat.text,
      nomor_telpon: _telpon.text,
      otp: "0",
      status_kepegawaian: _status,
      gaji: _gaji.text,
      bank: _bank,
      sales_respon: _respon.text,
      kondisi: "Pending",
    );

    await DbCustomerViewModel().insertData(model).then((value) {
      clearInput();
      Navigator.of(context).popUntil((route) => route.isFirst);
    });
  }

  void clearInput() {
    setState(() {
      _fotoPath = "";
      _nik.text = "";
      _nama.text = "";
      _dob.text = "";
      _alamat.text = "";
      _telpon.text = "";
      _otp.text = "";
      _status = "";
      _gaji.text = "";
      _bank = null;
      _respon.text = "";
      _foto = null;
    });
  }

  void handleRadio(int value) {
    if (value == 0) {
      setState(() {
        statusRadioValue = value;
        _status = "Aktif Bekerja";
      });
    } else if (value == 1) {
      setState(() {
        statusRadioValue = value;
        _status = "Pensiun";
      });
    }
  }

  Future getImageFromCamera() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    setState(() {
      if (pickedFile != null) _foto = File(pickedFile.path);
      _fotoPath = pickedFile.path;
    });
  }

  void datePicker(BuildContext _context) async {
    final DateTime picker = await showDatePicker(
        context: _context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900),
        lastDate: DateTime.now());
    if (picker != null) {
      setState(() {
        _dob.text = DateFormat('dd/MM/yyyy').format(picker);
      });
    }
  }
}
