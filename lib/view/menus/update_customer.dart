import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:soalmobiletest/component/costum_confirmation_dialog.dart';
import 'package:soalmobiletest/models/db_customer_model.dart';
import 'package:soalmobiletest/viewmodel/db_customer_viewmodel.dart';

class UpdateCustomer extends StatefulWidget {
  final DbCustomerModel data;
  UpdateCustomer({this.data});
  @override
  _UpdateCustomerState createState() => _UpdateCustomerState();
}

class _UpdateCustomerState extends State<UpdateCustomer> {
  TextEditingController _nik = new TextEditingController();
  TextEditingController _nama = new TextEditingController();
  TextEditingController _dob = new TextEditingController();
  TextEditingController _alamat = new TextEditingController();
  TextEditingController _telpon = new TextEditingController();
  TextEditingController _otp = new TextEditingController();
  TextEditingController _gaji = new TextEditingController();
  TextEditingController _respon = new TextEditingController();
  int statusRadioValue;
  String _status;
  String _bank;
  String _fotoPath;
  // File _foto;
  List itemBank = ["BNI", "BTPN", "BRI"];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nik.text = widget.data.nik;
    _nama.text = widget.data.nama;
    _dob.text = widget.data.tanggal_lahir;
    _alamat.text = widget.data.alamat;
    _telpon.text = widget.data.nomor_telpon;
    _otp.text = widget.data.otp;
    _status = widget.data.status_kepegawaian;
    _gaji.text = widget.data.gaji;
    _respon.text = widget.data.sales_respon;
    _fotoPath = widget.data.foto_path;
    _bank = widget.data.bank;
    if (widget.data.status_kepegawaian == "Aktif Bekerja") {
      setState(() {
        statusRadioValue = 0;
      });
    } else {
      setState(() {
        statusRadioValue = 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration:
              BoxDecoration(border: Border.all(width: 10, color: Colors.grey)),
          child: GestureDetector(
            child: ListView(
              padding: EdgeInsets.all(20),
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      GestureDetector(
                        // onTap: getImageFromCamera,
                        child: CircleAvatar(
                          radius: 40,
                          backgroundImage: FileImage(File(_fotoPath)),
                        ),
                      ),
                      // Text("Ambil Foto Selfie")
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("NIK"),
                      TextField(
                        // maxLength: 16,
                        keyboardType: TextInputType.number,
                        controller: _nik,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Nama"),
                      TextField(
                        controller: _nama,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Tanggal Lahir"),
                      TextField(
                        controller: _dob,
                        keyboardType: TextInputType.datetime,
                        decoration: InputDecoration(
                          suffixIcon: Icon(CupertinoIcons.calendar),
                        ),
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());
                          datePicker(context);
                        },
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Alamat"),
                      TextField(
                        controller: _alamat,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("No. Telpon"),
                      TextField(
                        controller: _telpon,
                        keyboardType: TextInputType.phone,
                      ),
                    ],
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(8.0),
                //   child: Column(
                //     crossAxisAlignment: CrossAxisAlignment.start,
                //     children: [
                //       Text("Kode OTP"),
                //       TextField(
                //         controller: _otp,
                //         keyboardType: TextInputType.number,
                //       ),
                //     ],
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Status Kepegawaian"),
                      Row(
                        children: [
                          new Radio(
                            value: 0,
                            groupValue: statusRadioValue,
                            onChanged: handleRadio,
                          ),
                          Text("Aktif Bekerja"),
                          SizedBox(
                            width: 5.0,
                          ),
                          new Radio(
                            value: 1,
                            groupValue: statusRadioValue,
                            onChanged: handleRadio,
                          ),
                          Text("Pensiun"),
                        ],
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Gaji Aktif/Pensiun"),
                      TextField(
                        controller: _gaji,
                        keyboardType: TextInputType.number,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Pembayaran Gaji dengan Bank"),
                      Container(
                        child: DropdownButton(
                          value: _bank,
                          items: itemBank.map((e) {
                            return DropdownMenuItem(
                              child: Text(e),
                              value: e,
                            );
                          }).toList(),
                          onChanged: (e) {
                            setState(() {
                              _bank = e;
                            });
                          },
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Sales Respon"),
                      TextField(
                        controller: _respon,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey)),
                      onPressed: () {
                        validasiInput();
                      },
                      child: Text(
                        "Update",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    TextButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey)),
                      onPressed: () {
                        var confirmationDialog = CustomConfirmationDialog(
                          title: "Apakah Anda Yakin Ingin Menghapus Data?",
                          message:
                              "Setiap data yang dihapus tidak dapat dikembalikan. Perhatikan kembali data yang akan dihapus.",
                          yes: "Delete",
                          no: "Cancel",
                          pressYes: () {
                            deleteCustomer();
                          },
                          pressNo: () {
                            Navigator.of(context).pop();
                          },
                        );
                        showDialog(
                            context: context,
                            builder: (_) => confirmationDialog);
                      },
                      child: Text(
                        "Delete",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void validasiInput() {
    if (_nik.text.isEmpty ||
        _fotoPath == null ||
        _nama.text.isEmpty ||
        _dob.text.isEmpty ||
        _alamat.text.isEmpty ||
        _telpon.text.isEmpty ||
        _otp.text.isEmpty ||
        _status == null ||
        _bank == null ||
        _gaji.text.isEmpty ||
        _respon.text.isEmpty) {
      Fluttertoast.showToast(
          msg: "Semua Data Harus Diisi",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER);
    } else {
      var confirmationDialog = CustomConfirmationDialog(
        title: "Apakah Anda Yakin Telah Mengedit Data Dengan Benar?",
        message:
            "Setiap data yang anda edit akan kami simpan dan mengubahnya di data kantor pusat. Perhatikan kembali data - data yang anda input.",
        yes: "Simpan Data",
        no: "Mengedit Lagi",
        pressYes: () {
          updateData();
        },
        pressNo: () {
          Navigator.of(context).pop();
        },
      );
      showDialog(context: context, builder: (_) => confirmationDialog);
    }
  }

  void updateData() async {
    DbCustomerModel model = new DbCustomerModel(
      id: widget.data.id,
      foto_path: _fotoPath,
      nik: _nik.text,
      nama: _nama.text,
      tanggal_lahir: _dob.text,
      alamat: _alamat.text,
      nomor_telpon: _telpon.text,
      otp: _otp.text,
      status_kepegawaian: _status,
      gaji: _gaji.text,
      bank: _bank,
      sales_respon: _respon.text,
      kondisi: widget.data.kondisi,
    );

    await DbCustomerViewModel().updateData(model).then((value) {
      clearInput();
      Navigator.of(context).popUntil((route) => route.isFirst);
    });
  }

  void deleteCustomer() async {
    DbCustomerModel model = new DbCustomerModel(
      id: widget.data.id,
      foto_path: _fotoPath,
      nik: _nik.text,
      nama: _nama.text,
      tanggal_lahir: _dob.text,
      alamat: _alamat.text,
      nomor_telpon: _telpon.text,
      otp: _otp.text,
      status_kepegawaian: _status,
      gaji: _gaji.text,
      bank: _bank,
      sales_respon: _respon.text,
      kondisi: widget.data.kondisi,
    );

    await DbCustomerViewModel().deleteData(model).then((value) {
      Navigator.of(context).popUntil((route) => route.isFirst);
    });
  }

  void clearInput() {
    setState(() {
      _fotoPath = "";
      _nik.text = "";
      _nama.text = "";
      _dob.text = "";
      _alamat.text = "";
      _telpon.text = "";
      _otp.text = "";
      _status = "";
      _gaji.text = "";
      _bank = null;
      _respon.text = "";
    });
  }

  // Future getImageFromCamera() async {
  //   final pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
  //   setState(() {
  //     if (pickedFile != null) _foto = File(pickedFile.path);
  //     _fotoPath = pickedFile.path;
  //   });
  // }

  void handleRadio(int value) {
    if (value == 0) {
      setState(() {
        statusRadioValue = value;
        _status = "Aktif Bekerja";
      });
    } else if (value == 1) {
      setState(() {
        statusRadioValue = value;
        _status = "Pensiun";
      });
    }
  }

  void datePicker(BuildContext _context) async {
    final DateTime picker = await showDatePicker(
        context: _context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900),
        lastDate: DateTime.now());
    if (picker != null) {
      setState(() {
        _dob.text = DateFormat('dd/MM/yyyy').format(picker);
      });
    }
  }
}
