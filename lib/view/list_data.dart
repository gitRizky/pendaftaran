import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:soalmobiletest/models/db_customer_model.dart';
import 'package:soalmobiletest/view/menus/add_customer.dart';
import 'package:soalmobiletest/view/menus/update_customer.dart';
import 'package:soalmobiletest/viewmodel/db_customer_viewmodel.dart';

class ListData extends StatefulWidget {
  @override
  _ListDataState createState() => _ListDataState();
}

class _ListDataState extends State<ListData> {
  List<DbCustomerModel> listCustomer = List<DbCustomerModel>.empty();
  TextEditingController _keywordNama = new TextEditingController();
  List itemKondisi = ["Accept", "Reject"];
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = new IOSInitializationSettings();
    var initSetttings = new InitializationSettings(android: android, iOS: iOS);
    flutterLocalNotificationsPlugin.initialize(initSetttings,
        onSelectNotification: onSelectNotification);
    tampilDataCustomer();
  }

  Future onSelectNotification(String payload) {
    debugPrint("payload : $payload");
    showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: new Text('Notification'),
        content: new Text('$payload'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              "Kunjungan Saya",
              style: TextStyle(color: Colors.white),
            ),
            actions: [
              Container(
                width: 70,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: TextFormField(
                    controller: _keywordNama,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Search",
                      hintStyle: TextStyle(color: Colors.white70),
                    ),
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                ),
              ),
              IconButton(
                  icon: Icon(
                    CupertinoIcons.search,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    cariData();
                  }),
            ],
          ),
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Expanded(
                  child: ListView.builder(
                      itemCount: listCustomer.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          padding: EdgeInsets.all(5.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                onTap: () {
                                  DbCustomerModel data = listCustomer[index];
                                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  UpdateCustomer(data: data)))
                                      .then((value) {
                                    setState(() {
                                      tampilDataCustomer();
                                    });
                                  });
                                },
                                child: Container(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      CircleAvatar(
                                        backgroundImage: FileImage(
                                          File(listCustomer[index].foto_path),
                                        ),
                                        radius: 30,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(listCustomer[index].nama),
                                          Text(
                                              "${listCustomer[index].nomor_telpon}"),
                                          Text(
                                              listCustomer[index].sales_respon),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                width: 100,
                                height: 50,
                                child: Center(
                                  child: DropdownButton(
                                    hint: Text(listCustomer[index].kondisi),
                                    value: null,
                                    items: itemKondisi.map((e) {
                                      return DropdownMenuItem(
                                        child: Text(e),
                                        value: e,
                                      );
                                    }).toList(),
                                    onChanged: (e) {
                                      DbCustomerModel data =
                                          listCustomer[index];
                                      if (data.kondisi != e) {
                                        updateData(data, e);
                                      }
                                    },
                                  ),
                                  // Text(listCustomer[index].kondisi),
                                ),
                                color: listCustomer[index].kondisi == "Pending"
                                    ? Colors.yellowAccent
                                    : listCustomer[index].kondisi == "Accept"
                                        ? Colors.amberAccent
                                        : Colors.red,
                              )
                            ],
                          ),
                        );
                      }),
                )
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(
              CupertinoIcons.plus,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AddCustomer()))
                  .then((value) {
                setState(() {
                  tampilDataCustomer();
                });
              });
            },
          ),
        ),
      ),
    );
  }

  tampilDataCustomer() async {
    await DbCustomerViewModel().readData().then((value) {
      setState(() {
        listCustomer = value;
      });
    });
  }

  cariData() async {
    if (_keywordNama.text.isEmpty) {
      tampilDataCustomer();
    } else {
      await DbCustomerViewModel().searchData(_keywordNama.text).then((value) {
        setState(() {
          _keywordNama.text = "";
          listCustomer = value;
        });
      });
    }
  }

  void updateData(DbCustomerModel data, String e) async {
    DbCustomerModel model = new DbCustomerModel(
      id: data.id,
      foto_path: data.foto_path,
      nik: data.nik,
      nama: data.nama,
      tanggal_lahir: data.tanggal_lahir,
      alamat: data.alamat,
      nomor_telpon: data.nomor_telpon,
      otp: data.otp,
      status_kepegawaian: data.status_kepegawaian,
      gaji: data.gaji,
      bank: data.bank,
      sales_respon: data.sales_respon,
      kondisi: e,
    );

    await DbCustomerViewModel().updateData(model).whenComplete(() {
      showNotification(model.nama, model.kondisi);
      tampilDataCustomer();
    });
  }

  showNotification(String nama, String kondisi) async {
    var android = new AndroidNotificationDetails(
        'channel id', 'channel NAME', 'CHANNEL DESCRIPTION',
        priority: Priority.high, importance: Importance.max);
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android: android, iOS: iOS);
    await flutterLocalNotificationsPlugin.show(
        0, '$nama', 'telah $kondisi', platform,
        payload: 'Peminjaman $nama telah di-$kondisi');
  }
}
